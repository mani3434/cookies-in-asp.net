﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Cookies.Controllers
{
    public class cookiesController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public IActionResult writeCookie(string cookiename, string cookievalue, bool IsPersistent)
        {
            CookieOptions cookies = new CookieOptions();
            cookies.Expires = DateTime.Now.AddDays(1);
            if (IsPersistent)
            {
                Response.Cookies.Append(cookiename, cookievalue, cookies);
            }
            else
            {
                Response.Cookies.Append(cookiename, cookievalue);
            }
            ViewBag.message = "Cookie add successfully";
            return View("Index");
        }

        public IActionResult readCookie()
        {
            ViewBag.cookievalue1 = Request.Cookies["username"].ToString();
            ViewBag.cookievalue2 = Request.Cookies["password"].ToString();

            return View();
        }
    }
}